import axios from "axios";
import { useEffect, useState, PureComponent } from "react";
import { Container, Spinner } from "react-bootstrap";
import {
    Tooltip,
    Treemap,
    ResponsiveContainer,
  } from "recharts";
  import "./Visualizations.css";

const COLORS = ['#8889DD', '#9597E4', '#8DC77B', '#A5D297', '#E2CF45', '#F8C12D'];

interface Props {
    root?: any;
    depth?: any; 
    x?: any;
    y?: any; 
    width?: any; 
    height?: any; 
    index?: any;
    payload?: any; 
    colors: string[];
    name?: any;
}

class CustomizedContent extends PureComponent<Props> {
    render() {
      const { root, depth, x, y, width, height, index, colors, name } = this.props;
  
      return (
        <g>
          <rect
            x={x}
            y={y}
            width={width}
            height={height}
            style={{
              fill: depth < 2 ? colors[Math.floor((index / root.children.length) * 6)] : 'none',
              stroke: '#fff',
              strokeWidth: 2 / (depth + 1e-10),
              strokeOpacity: 1 / (depth + 1e-10),
            }}
          />
          {depth === 1 ? (
            <text x={x + width / 2} y={y + height / 2 + 7} textAnchor="middle" fill="#fff" fontSize={14}>
              {name}
            </text>
          ) : null}
          {depth === 1 ? (
            <text x={x + 4} y={y + 18} fill="#fff" fontSize={16} fillOpacity={0.9}>
              {index + 1}
            </text>
          ) : null}
        </g>
      );
    }
}

const getShortenedName = (name: string) => {
    let shortName = name
    if (name === "Near Ash River Visitor Center, Ash River Community, Ash River Trail, Near Kabetogama Lake Visitor Center") {
        shortName = "Ash River";
    } else if (name.includes("Trailhead")) {
        shortName = name.replace("Trailhead", "");
    } else if (name.includes(" Trail")) {
        shortName = name.replace("Trail", "")
    } else if (name.includes("Bike to ")) {
        shortName = name.replace("Bike to ", "");
    } else if (name === "Wildcat Beach, Point Reyes National Seashore") {
        shortName = name.split(",")[0];
    } else if (name === "On the Kabetogama peninsula") {
        shortName = "Kabetogama";
    }
    return shortName;
};

const LongestTrailsChart = () => {
    const [chartData, setChartData] = useState([]);
    const [loading, setLoading] = useState(true);

    const getTrailsData = async () => {
        axios
        .get(`https://api.trailmixapp.me/trail?sort=-distance`, {
            headers: {
            "Content-Type": "application/vnd.api+json",
            Accept: "application/vnd.api+json",
            },
        })
        .then((res: any) => {
            var trailData = res.data.data;
            var chartDataList: any = [];

            for (let i = 0; i < trailData.length; i++) {
                var currPark = trailData[i];
                var trailLength = currPark.attributes.distance;
                var name = currPark.attributes.name;
                name = getShortenedName(name);
                chartDataList.push({
                    name: name,
                    trail_length: parseInt(trailLength),
                });
            }
        setChartData(chartDataList);
        setLoading(false);
        });
    };

    useEffect(() => {
        getTrailsData();
    }, []);

    return (
        <div>
        {loading ? (
            <Container className="visualizationBox">
            <Spinner animation="border" />
            </Container>
        ) : (
            <Container className="visualizationBox">
            <h3 className="visualizationTitle">
                Longest Trails
            </h3>
            <ResponsiveContainer width="99%" height="99%">
                <Treemap
                    width={400}
                    height={200}
                    data={chartData}
                    dataKey="trail_length"
                    aspectRatio={4 / 3}
                    stroke="#fff"
                    fill="#8884d8"
                    content={<CustomizedContent colors={COLORS} />}
                >
                <Tooltip label="trail_length" labelFormatter={() => "Trail Length"}/>
                </Treemap>
            </ResponsiveContainer>
            </Container>
        )}
        </div>
    );

};

export default LongestTrailsChart;

