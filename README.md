# Chemicals Near Me

Phase 1 Git SHA: 6a711fb52b1924840e128a94315baeb6a146a0db <br/>
Phase 2 Git SHA: e8a1c5757ad496e771a3be9bfbd6af2308cddc92 <br/>
Phase 3 Git SHA: e195a7500bf077b47cb82249e3629ebc5f1463a1 <br/>
Phase 4 Git SHA: 3777c74e5fb8fd006e1b8e1a916fe953a7884170 <br/>

## The Team

| Name              | EID       | GitLab ID         |
| ----------------- | --------- | ----------------- |
| Francis Tran      | ft4225    | @francistran1     |
| Fedor Aglyamov    | fsa385    | @FedorAglyamov    |
| Vivian Ta         | vqt86     | @vivianta         |
| Timothy Keeton    | tck548    | @tkeeton          |
| Lorenzo Martinez  | lam5985   | @lorenzomtz       |

## Website

[Chemicals Near Me](https://www.chemicalsnear.me)

## Phase One Estimated Completion Time

| Name              | Estimate  | Actual    |
| ----------------- | --------- | --------- |
| Francis Tran      | 10 hours  | 15 hours  |
| Fedor Aglyamov    | 20 hours  | 18 hours  |
| Vivian Ta         | 12 hours  | 15 hours  |
| Timothy Keeton    | 12 hours  | 15 hours  |
| Lorenzo Martinez  | 12 hours  | 15 hours  |

*** Phase One Leader: Lorenzo Martinez ***

## Phase Two Estimated Completion Time

| Name              | Estimate  | Actual    |
| ----------------- | --------- | --------- |
| Francis Tran      | 20 hours  | 40 hours  |
| Fedor Aglyamov    | 20 hours  | 33 hours  |
| Vivian Ta         | 20 hours  | 30 hours  |
| Timothy Keeton    | 20 hours  | 30 hours  |
| Lorenzo Martinez  | 20 hours  | 30 hours  |

*** Phase Two Leader: Timothy Keeton ***

## Phase Three Estimated Completion Time

| Name              | Estimate  | Actual    |
| ----------------- | --------- | --------- |
| Francis Tran      | 20 hours  | 25 hours  |
| Fedor Aglyamov    | 20 hours  | 25 hours  |
| Vivian Ta         | 20 hours  | 25 hours  |
| Timothy Keeton    | 20 hours  | 25 hours  |
| Lorenzo Martinez  | 20 hours  | 25 hours  |

*** Phase Three Leader: Francis Tran ***

## Phase Four Estimated Completion Time

| Name              | Estimate  | Actual    |
| ----------------- | --------- | --------- |
| Francis Tran      | 10 hours  | 10 hours  |
| Fedor Aglyamov    | 15 hours  | 15 hours  |
| Vivian Ta         | 10 hours  | 10 hours  |
| Timothy Keeton    | 10 hours  | 10 hours  |
| Lorenzo Martinez  | 15 hours  | 15 hours  |

*** Phase Four Leader: Fedor Aglyamov ***

## RESTful API Documentation

[Documentation](https://documenter.getpostman.com/view/17744348/UVCBA4ns)

## GitLab Pipelines

[Pipelines](https://gitlab.com/cs373-11am-group5/chemicals-near-me/-/pipelines)

## Notes

Using different APIs for visualizations is definitely a learning experience.
